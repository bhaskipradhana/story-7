from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import request
from importlib import import_module
from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from django.conf import settings

from .views import *
from .models import *


# Create your tests here.
class story6_test(TestCase):
    def test_url(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_url_is_notexist(self):
        response = Client().get('/notexist/')
        self.assertEqual(response.status_code, 404)

    def test_status_using_story7_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'story7.html')