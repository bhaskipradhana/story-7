$(document).ready(function(){

    var allPanels = $('.accordion > dd').hide();
    var color = ["#08274D", "#F9F2F2"];
    var i = -1;
        
    $('.accordion > dt > a').click(function() {
        allPanels.slideUp();
    
        if ($(this).parent().next().css('display') == 'none') {
            $(this).parent().next().slideDown();
        }else{
            $(this).parent().next().slideUp(); 
        }
        return false;
    }); 
    
    $("input").click(function(){
        i = i < color.length-1 ?  ++i : 0;
        //awalnya putih
        $("body").css({"background" : color[i]});
        $("a").css({"color" : color[i]});

        //awalnya item
        $("dt").css({"background" : color[(i+1)%2]});
        $("h2, p, li").css({"color" : color[(i+1)%2]});
    });
    
    
    
    });
// // var acc = document.getElementsByClassName("accordion");
// // var i;

// // for (i = 0; i < acc.length; i++) {
// //   acc[i].onclick = function() {
// //     this.classList.toggle("active");
// //     var panel = this.nextElementSibling;
// //     if (panel.style.maxHeight) {
// //       panel.style.maxHeight = null;
// //     } else {
// //       panel.style.maxHeight = panel.scrollHeight + "px";
// //     }
// //   }
// // }